# Script to be used by cx_freeze (if correctly installed).
# Can run using something like:
# "python setup.py build"
# and will generate an exe under build/<version>

from cx_Freeze import setup, Executable
# includes = ["sip","re","PyQt4.QtCore","PyQt4.QtGui"]
includes = ["scipy.sparse","scipy.sparse.sparsetools.csr","scipy.sparse.sparsetools._csr"]

exe = Executable(
    script="run_EVIL2.py",
    base="Win32GUI"
    )
setup(
    options = {"build_exe": {"includes":includes}},
    executables = [exe], requires=['numpy']
    )
