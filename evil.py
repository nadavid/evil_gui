from datetime import datetime
from multiprocessing import Queue
from multiprocessing.process import Process
from queue import Empty
import sys

from PyQt4 import QtCore as QtC
from PyQt4 import QtGui as QtG
from PyQt4.QtCore import pyqtSlot, pyqtSignal
from PyQt4.uic import loadUi
from serial.serialutil import SerialException
import numpy as np
from communication import connection as c
from communication.connection import ConnectionException
from pyqtgraph.graphicsItems.InfiniteLine import InfiniteLine

GUI_VERSION = "1.40"

PID_RST_N = 1 << 0
RAMP_RST_N = 1 << 1
OUTPUT_SEL = 1 << 2
PID_POLARITY = 1 << 3
ADC_PDWN = 1 << 4
LD_ON = 1 << 5

VENDOR_ID = '0403'
MODEL_ID = '6010'
BUS_ID = '01'

STREAMING_CHANNELS = [
    'ADC A (error signal)',
    'ADC B (cavity transmission)',
    'ramp output',
    'PID output (MSB)',
    'PID output (LSB)',
    'PID output (slow lowpass)',
    'PID output (filter difference)',
    'NONE'
]

logfile = None

class Setting:
    def __init__(self, register_number, widget, signed=False, handler=None):
        """As widget, we want to use the SpinBox. This is a convention.
        Also: One nifty feature. If widget is just a integer value, this will be sent and written to the UI"""
        self.register_number = register_number
        self.widget = widget
        self.signed = signed
        self.handler = handler
        self.name = None

    def value(self, new=None):
        if new is not None and self.handler:
            self.handler(self, new)
        else:
            if isinstance(self.widget, QtG.QWidget):
                if new is not None:
                    self.widget.setValue(new)
                    return self
                else:
                    val = self.widget.value()
            else:
                if new is not None:
                    self.widget = new
                    return self
                else:
                    val = self.widget
            return val


def communications_worker(op_queue, result_queue, portname):
    streaming = False
    ramp_trigger = False
    connection = c.open_serial_port(portname)
    while True:
        # basically we do the same here over and over:
        # we look into the op queue whether we got a new command (e.g. register read/write)
        # - if we do, it is executed and the result is pushed with the original command to the result_queue.
        # - if not, and if streaming is enabled, we try to grab streaming samples and push them to the result queue
        try:
            command = op_queue.get(timeout=0.01)
            if not isinstance(command, list):
                if type(command) == int:
                    command = [command]
                else:
                    command = list(command)

            if command[0] == Communication.OP_STREAM_START:
                streaming = command[1:]
            elif command[0] == Communication.OP_STREAM_STOP:
                streaming = False
            elif command[0] == Communication.OP_WRITE:         # command, signed, register, value
                if command[1]:
                    c.set_reg_s(command[2], command[3], connection)
                else:
                    c.set_reg_u(command[2], command[3], connection)
                result_queue.put(command)
            elif command[0] == Communication.OP_READ:          # command, setting, signed, register
                if command[2]:
                    val = c.get_reg_s(command[3], connection)
                else:
                    val = c.get_reg_u(command[3], connection)

                command.append(val)
                result_queue.put(command)
            elif command[0] == Communication.OP_RAMP_TRIGGER_SET:
                ramp_trigger = command[1]
            elif command[0] == Communication.OP_TERMINATE:
                result_queue.put(command)
                break
            else:
                print('communication worker received unknown command')
        except Empty:
            # we got no specific command, so we'll just get a streaming packet
            if streaming:
                if result_queue.qsize() < 10:
                    data = c.get_stream(streaming[0], streaming[2], connection, streaming[1])

                    if ramp_trigger:
                        trig_point = c.get_reg_u(16, connection)
                        data = data[streaming[2]-trig_point:]
                    result_queue.put((Communication.OP_STREAM_DATA, data))
                else:
                    print('WARNING: Dropped streaming packet since the queue was filling up!')


class Communication(QtC.QObject):
    """The purpose of this class is to handle the complete communication with the FPGA.

    All communication will be queued and executed sequentially since the FPGA is not capable to handle asynchronous I/O
    (e.g. streaming ADC data and writing to a register at the same time)."""

    OP_WRITE = 0               # command, signed, register, value
    OP_READ = 1                # command, settingname, signed, register
    OP_STREAM_START = 2        # command, register, signed
    OP_STREAM_STOP = 3         # command, register
    OP_STREAM_DATA = 4
    OP_RAMP_TRIGGER_SET = 5    # command, bool
    OP_TERMINATE = 100         # command

    start_result_receiver = pyqtSignal()
    read_setting = pyqtSignal(str, int)
    got_stream_data = pyqtSignal(np.ndarray)

    def __init__(self, *kargs, **kwargs):
        QtC.QObject.__init__(self, *kargs, **kwargs)

        self.op_queue = Queue()
        self.result_queue = Queue()
        self.process = None

        self.start_result_receiver.connect(self.result_receiver)

    def start_worker(self, portname):
        self.terminate_worker()
        self.start_result_receiver.emit()
        self.process = Process(target=communications_worker, args=(self.op_queue, self.result_queue, portname))
        self.process.start()

    def terminate_worker(self):
        if self.process:
            self.op_queue.put(self.OP_TERMINATE)
            self.process.join()
            exitcode = self.process.exitcode
            self.process = None

            return exitcode

    def write(self, setting, value=None):
        if value is None:
            value = setting.value()
        self.op_queue.put((self.OP_WRITE, setting.signed, setting.register_number, value))

    def read(self, setting):
        self.op_queue.put((self.OP_READ, setting.name, setting.signed, setting.register_number))

    def start_streaming(self, register, signed, sample_count):
        self.op_queue.put((self.OP_STREAM_START, register, signed, sample_count))

    def stop_streaming(self):
        self.op_queue.put(self.OP_STREAM_STOP)

    def set_ramp_trigger(self, on):
        self.op_queue.put((self.OP_RAMP_TRIGGER_SET, on))

    @pyqtSlot()
    def result_receiver(self):
        while True:
            data = self.result_queue.get()
            if data[0] == self.OP_READ:
                self.read_setting.emit(data[1], data[4])
            elif data[0] == self.OP_STREAM_DATA:
                self.got_stream_data.emit(data[1])
            elif data[0] == self.OP_TERMINATE:
                break


class Control(QtG.QWidget):
    def __init__(self):
        QtG.QWidget.__init__(self)
        
        loadUi('gui/evil_control.ui', self)

        self.logfile = None
        self.current_stream_data = None

        self.polarization = False
        self.sweeping = False

        # add valid serial ports and disable settings tab
        try:
            # noinspection PyPackageRequirements, PyUnresolvedReferences
            import pyudev
            if pyudev.__version_info__[0] > 0 or pyudev.__version_info__[1] < 16:
                raise ImportError

            self.device_context = pyudev.Context()
            self.device_monitor = pyudev.Monitor.from_netlink(self.device_context)
            self.device_monitor.filter_by(subsystem='tty')
            self.device_observer = pyudev.MonitorObserver(self.device_monitor, callback=self.device_changed)
            self.device_observer.start()

            ports = []
            for device in self.device_context.list_devices(subsystem='tty',
                                                           ID_VENDOR_ID=VENDOR_ID,
                                                           ID_MODEL_ID=MODEL_ID):
                if device['ID_USB_INTERFACE_NUM'] == '01':
                    ports.append(device.device_node)
        except ImportError:
            print('unable to monitor device changeself. If on linux, install pyudev >= 0.16')
            ports = c.find_out_serial_ports()
        [self.portNameComboBox.addItem(port) for port in ports]
        self.tabWidget.setTabEnabled(1, False)
        self.cancel_reconnection_timer = False

        # getting the communications system up and running
        self.communication_thread = QtC.QThread(self)
        self.communication = Communication()
        self.communication.moveToThread(self.communication_thread)
        self.communication_thread.start()
        self.communication.read_setting.connect(self.got_fpga_value)
        self.communication.got_stream_data.connect(self.got_stream_data)

        self.streamingChannelComboBox.clear()
        for i in range(8):
            self.streamingChannelComboBox.addItem(STREAMING_CHANNELS[i])
        self.streaming = False
        self.change_child_enabled(self.streamingLayout, False)

        self.saveShotButton.setIcon(QtG.QIcon('gui/images/shot.png'))

        # Connect QT signals
        self.connectButton.clicked.connect(self.connect_button)
        self.streamingButton.clicked.connect(self.toggle_streaming)

        self.sweepButton.clicked.connect(self.run_sweep)
        self.flipPolarizationButton.clicked.connect(self.flip_polarization)
        self.resetPidButton.clicked.connect(self.pid_reset)
        self.loadFpgaValuesButton.clicked.connect(self.reload_fpga_values)
        self.loadButton.clicked.connect(self.load_data)
        self.saveButton.clicked.connect(self.save_data)
        self.rampTriggerCheckBox.clicked.connect(self.ramp_trigger_clicked)
        self.streamingChannelComboBox.currentIndexChanged.connect(self.start_streaming)
        self.relockingEnabledCheckBox.clicked.connect(self.toggle_relocking)
        self.logfileButton.clicked.connect(self.choose_logfile_button)
        self.saveShotButton.clicked.connect(self.single_shot_button)

        # Collecting all available settings
        self.settings = {
            'systemControl': Setting(0, 0, False, handler=self.systemcontrol_valuehandler),

            'centerSpinBox': Setting(1, self.centerSpinBox, True),
            'rangeSpinBox': Setting(2, self.rangeSpinBox, False),
            'frequencySpinBox': Setting(3, self.frequencySpinBox, False),
            'pGainSpinBox': Setting(6, self.pGainSpinBox, False),
            'iGainSpinBox': Setting(7, self.iGainSpinBox, False),
            'dGainSpinBox': Setting(8, self.dGainSpinBox, False),
            'inputOffsetSpinBox': Setting(4, self.inputOffsetSpinBox, True),
            'outputOffsetSpinBox': Setting(5, self.outputOffsetSpinBox, True),
            'filterResponseSpinBox': Setting(9, self.filterResponseSpinBox, True),
            'thresholdSpinBox': Setting(10, self.thresholdSpinBox, False),
            'ttlExpSpinBox': Setting(11, self.ttlExpSpinBox, False),

            'hardwareSampleDelaySpinBox': Setting(14, self.hardwareSampleDelaySpinBox, False),
            'hardwareSampleCountSpinBox': Setting(15, self.hardwareSampleCountSpinBox, False),
        }
        self.settings['thresholdSpinBox'].widget.valueChanged.connect(self.filter_threshold_changed)
        self.settings['hardwareSampleCountSpinBox'].widget.valueChanged.connect(self.streaming_settings_changed)
        self.settings['hardwareSampleDelaySpinBox'].widget.valueChanged.connect(self.streaming_settings_changed)
        for key, setting in self.settings.items():
            setting.name = key

        self.plotWidget.getPlotItem().setRange(xRange=(0, 10000), yRange=(-600, 600))
        self.plotWidget.getPlotItem().setLabel('bottom', 'time', 's')

        # these are things that are added to the plot of a particular streaming channel
        # e.g. the threshold line to the filtered difference streaming channel
        self.extra_drawingitems = {
            6: {
                'threshold': InfiniteLine(1, angle=0)
            }
        }
        self.currently_displayed_extraitems = [] # so we can remove them easily when switching channels

        # connect our settings value changed signal
        for key, setting in self.settings.items():
            if isinstance(setting.widget, QtG.QWidget):
                setting.widget.valueChanged.connect(self.setting_changed)

        self.log("EVIL GUI %s" % GUI_VERSION)

    def device_changed(self, device):
        # removing action (maybe disconnecting)
        if device.action == 'remove':
            if self.communication.process and device.device_node == self.portNameComboBox.currentText():
                self.connectButton.clicked.emit(True)

            for i in range(self.portNameComboBox.model().rowCount()):
                if device.device_node == self.portNameComboBox.itemText(i):
                    self.portNameComboBox.removeItem(i)
        if device.action == 'add':
            try:
                if device['ID_VENDOR_ID'] == VENDOR_ID and device['ID_MODEL_ID'] == MODEL_ID \
                        and device['ID_USB_INTERFACE_NUM'] == BUS_ID:
                    self.portNameComboBox.addItem(device.device_node)
            except KeyError:
                pass
        self.portNameComboBox.update()

    def change_child_enabled(self, start, enabled):
        for i in range(start.count()):
            item = start.itemAt(i)
            if item.widget():
                item = item.widget()

            # ignore the save shot layout
            if item is self.saveShotLayout:
                continue

            if item.layout():
                self.change_child_enabled(item.layout(), enabled)
            elif getattr(item, 'setEnabled', False):
                item.setEnabled(enabled)

    def systemcontrol_valuehandler(self, setting, new):
        """This method will be called when the systemcontrol register is changed.
        The job is to update the UI state accordingly"""
        setting.widget = new

        self.relocking_enabled = (new & LD_ON) == LD_ON
        self.relockingEnabledCheckBox.setChecked(self.relocking_enabled)

        self.polarization = (new & PID_POLARITY) == PID_POLARITY
        if self.polarization:
            self.flipPolarizationButton.setStyleSheet('QPushButton {color: blue}')
        else:
            self.flipPolarizationButton.setStyleSheet('QPushButton {color: green}')

        SWEEPING_MASK = PID_RST_N | RAMP_RST_N | OUTPUT_SEL
        SWEEPING_STATE = RAMP_RST_N | OUTPUT_SEL
        CONTROLLING_STATE = PID_RST_N

        self.sweeping = (new & SWEEPING_MASK) == SWEEPING_STATE
        controlling = (new & SWEEPING_MASK) == CONTROLLING_STATE

        if self.sweeping or controlling:
            if self.sweeping:
                self.sweepButton.setText("Sweeping")
                self.sweepButton.setStyleSheet('QPushButton {color: green}')
            else:
                self.sweepButton.setText("Controlling")
                self.sweepButton.setStyleSheet('QPushButton {color: blue}')
        else:
            self.log("found some undefined system state %d" % new)

    @pyqtSlot()
    def ramp_trigger_clicked(self):
        self.communication.set_ramp_trigger(self.rampTriggerCheckBox.isChecked())

    @pyqtSlot()
    def filter_threshold_changed(self):
        self.extra_drawingitems[6]['threshold'].setValue(self.thresholdSpinBox.value())

    @pyqtSlot()
    def streaming_settings_changed(self):
        dt = 1/self.settings['hardwareSampleDelaySpinBox'].widget.sample_rate()/1000
        count = self.settings['hardwareSampleCountSpinBox'].value()
        self.plotWidget.getPlotItem().setXRange(0, dt*count)

        self.start_streaming()

    @pyqtSlot()
    def connect_button(self):
        if self.communication.process:
            self.close_connection()
        else:
            if self.connectButton.text() == 'Cancel waiting':
                # we are currently in a device waiting cycle which the user wants to cancel
                self.cancel_reconnection_timer = True
                self.connectButton.setEnabled(False)
            else:
                self.evil_connect()

    @pyqtSlot()
    def choose_logfile_button(self):
        filename = QtG.QFileDialog.getSaveFileName(self, directory="unnamed.evl",
                                                   filter="EVIL logfile (*.evl);;All files(*)")

        try:
            if self.logfile is not None:
                self.logfile.close()
            self.logfile = open(filename, 'wb')
            self.log("Streaming data will be written to %s" % filename)
        except Exception as e:
            self.log(e)

    @pyqtSlot()
    def single_shot_button(self):
        np.save(self.logfile, self.current_stream_data)

    @pyqtSlot()
    def start_streaming(self):
        channel = int(self.streamingChannelComboBox.currentIndex())
        self.plotWidget.getPlotItem().clear()
        if channel in self.extra_drawingitems:
            for i in self.extra_drawingitems[channel].values():
                self.plotWidget.getPlotItem().addItem(i)

        if self.streaming:
            self.communication.start_streaming(channel, True, self.settings['hardwareSampleCountSpinBox'].value())

    @pyqtSlot()
    def toggle_streaming(self):
        if self.streaming:
            self.communication.stop_streaming()
            self.streamingButton.setText('Start Streaming')
        else:
            self.streamingButton.setText('Stop Streaming')
        #self.change_child_enabled(self.streamingConfigLayout, self.streaming)
        self.streaming = not self.streaming
        self.saveShotButton.setEnabled(self.streaming)
        self.start_streaming()  # this only starts if self.streaming is true

    @pyqtSlot(str, float)
    def got_fpga_value(self, name, value):
        self.settings[name].value(value)

    @pyqtSlot()
    def reload_fpga_values(self):
        for setting in self.settings.values():
            self.communication.read(setting)
        self.log("FPGA values requested")

    @pyqtSlot(np.ndarray)
    def got_stream_data(self, data):
        channel = int(self.streamingChannelComboBox.currentIndex())
        data_l = len(data)
        dt = 1/self.settings['hardwareSampleDelaySpinBox'].widget.sample_rate()/1000
        xdata = np.linspace(0, dt*data_l, data_l)

        self.current_stream_data = np.vstack((xdata, 4*data))

        if len(data) > 0:
            self.plotWidget.getPlotItem().plot(xdata, 4*data, clear=True)

            if channel in self.extra_drawingitems:
                for i in self.extra_drawingitems[channel].values():
                    self.plotWidget.getPlotItem().addItem(i)

    @pyqtSlot()
    def pid_reset(self):
        self.pid_off()
        self.pid_on()
        self.log("PID reset")

    @pyqtSlot()
    def toggle_relocking(self):
        system_control = self.settings['systemControl'].value()
        if self.relocking_enabled:
            system_control = (system_control & ~LD_ON)
        else:
            system_control = (system_control | LD_ON)

        self.settings['systemControl'].value(system_control)
        self.communication.write(self.settings['systemControl'])

    @pyqtSlot()
    def flip_polarization(self):
        system_control = self.settings['systemControl'].value()

        pid_was_on = self.pid_off()
        if self.polarization:
            system_control = (system_control & ~PID_POLARITY)  # Flip Polarity
        else:
            system_control = (system_control | PID_POLARITY)  # Flip Polarity
        self.settings['systemControl'].value(system_control)
        self.communication.write(self.settings['systemControl'])
        if pid_was_on:
            self.pid_on()

    @pyqtSlot()
    def run_sweep(self):
        system_control = self.settings['systemControl'].value()
        if self.sweeping:
            system_control = (system_control | PID_RST_N)    # PID on
            system_control = (system_control & ~RAMP_RST_N)  # Ramp off
            system_control = (system_control & ~OUTPUT_SEL)  # Switch output to PID
        else:
            system_control = (system_control & ~PID_RST_N)   # PID off
            system_control = (system_control | RAMP_RST_N)   # Ramp on
            system_control = (system_control | OUTPUT_SEL)   # Switch output to ramp
        self.settings['systemControl'].value(system_control)
        self.communication.write(self.settings['systemControl'])

    @pyqtSlot()
    def setting_changed(self):
        name = self.sender().objectName()
        if name in self.settings:
            s = self.settings[name]
            self.communication.write(s)

    @pyqtSlot()
    def load_data(self):
        filename = QtG.QFileDialog.getOpenFileName(self, filter="EVIL file (*.evf);;All files(*)")
        try:
            with open(filename, 'r') as f:
                if not f.readline().startswith('EVILfile'):
                    raise Exception('<b>Invalid file format</b>')
                for line in f:
                    objname, objval = line.split("\t")
                    self.settings[objname].value(int(objval))
                    self.communication.write(self.settings[objname])
            self.log("Loaded data from file %s" % filename)
        except Exception as e:
            self.log(e)

    @pyqtSlot()
    def save_data(self):
        filename = QtG.QFileDialog.getSaveFileName(self, directory="unnamed.evf",
                                                   filter="EVIL file (*.evf);;All files(*)")
        try:
            with open(filename, 'w') as f:
                f.write("EVILfile\t%s\n" % GUI_VERSION)
                for name, setting in self.settings.items():
                    line = name + "\t" + str(setting.value()) + "\n"
                    f.write(line)
            self.log("Saved data to file %s" % filename)
        except Exception as e:
            self.log(e)

    def pid_off(self):
        s = self.settings["systemControl"]

        pid_was_on = (s.value() & PID_RST_N) == PID_RST_N
        s.value(s.value() & ~PID_RST_N)  # PID off
        self.communication.write(s)

        return pid_was_on

    def pid_on(self):
        s = self.settings["systemControl"]
        s.value(s.value() | PID_RST_N)  # PID on
        self.communication.write(s)

    @pyqtSlot()
    def evil_connect(self):
        if self.cancel_reconnection_timer:
            # user canceled waiting - do nothing
            self.cancel_reconnection_timer = False
            self.connectButton.setText('Connect')
            self.connectButton.setEnabled(True)
            self.portNameComboBox.setEnabled(True)
            self.log('waiting cancelled')
            return

        try:
            try:
                self.portNameComboBox.setEnabled(False)
                portname = self.portNameComboBox.currentText()
                connection = c.open_serial_port(portname)
            except SerialException:
                self.log("waiting for device (10 seconds)...")

                self.connectButton.setText("Cancel waiting")
                # noinspection PyTypeChecker,PyCallByClass
                QtC.QTimer.singleShot(10000, self.evil_connect)
                return

            version_message = c.version_check(connection, GUI_VERSION)
            if version_message:
                self.log(version_message)

            self.connectButton.setText("Close Connection")
            connection.close()
            self.communication.start_worker(portname)

            self.reload_fpga_values()

            # set up streaming
            self.streaming_settings_changed()

            self.ramp_trigger_clicked()

            self.tabWidget.setTabEnabled(1, True)
            self.change_child_enabled(self.streamingLayout, True)
            self.log("Connection Ready")
        except ConnectionException as e:
            self.portNameComboBox.setEnabled(True)
            self.log(e)
        except Exception as e:
            self.communication.terminate_worker()
            self.log(repr(e))
            self.portNameComboBox.setEnabled(True)
            raise e

    def close_connection(self):
        self.tabWidget.setTabEnabled(1, False)
        self.tabWidget.setCurrentIndex(0)
        if self.streaming:
            self.toggle_streaming()
        self.change_child_enabled(self.streamingLayout, False)
        self.communication.terminate_worker()

        self.connectButton.setText("Connect")
        self.portNameComboBox.setEnabled(True)
        self.log("Connection Closed")

    def log(self, message):
        self.messageLog.append('<i>%s:</i>&nbsp;&nbsp;&nbsp; %s'
                               % (datetime.now().strftime("%Y-%m-%d  %H:%M:%S"), message))

    # noinspection PyUnusedLocal
    def closeEvent(self, event):
        if self.logfile is not None:
            self.logfile.close()
        self.communication.terminate_worker()
        #self.communication_thread.terminate()

if __name__ == '__main__':
    app = QtG.QApplication(sys.argv)
    window = Control()
    window.show()
    sys.exit(app.exec_())
