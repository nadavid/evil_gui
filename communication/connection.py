import platform
import serial
import struct

import numpy as np


class ConnectionException(Exception):
    pass

def set_reg_u(reg, value, serport):
    bs = struct.pack('<BH', # byte, then 16b int, little-endian
                     ((0x1 & 0x7) << 5) | (reg & 0x1f),
                     value)
    serport.write(bs)

def set_reg_s(reg, value, serport):
    bs = struct.pack('<Bh', # byte, then 16b int, little-endian
                     ((0x1 & 0x7) << 5) | (reg & 0x1f),
                     value)
    serport.write(bs)
    
def get_reg_u(reg, serport):
    bs = struct.pack('<B',
                     ((0x0 & 0x7) << 5) | (reg & 0x1f)
                     )
    serport.write(bs)
    try:
        return struct.unpack('H',serport.read(2))[0]
    except:
        raise IOError("serial read failed or timed out")

def get_reg_s(reg, serport):
    bs = struct.pack('<B',
                     ((0x0 & 0x7) << 5) | (reg & 0x1f)
                     )
    serport.write(bs)
    try:
        return struct.unpack('h',serport.read(2))[0]
    except:
        raise IOError("serial read failed or timed out")

def get_stream(reg, num, serport, signed):
    dtype = 'uint8'
    if signed: dtype = 'int8'

    bs=struct.pack('<B', ((0x2 & 0x7) << 5) | (reg & 0x1f))
    serport.write(bs)
    try:
        return np.array(bytearray(serport.read(num)),dtype=dtype).astype('int16')
    except:
        raise IOError("streaming serial read failed or timed out")

#############################################################

#############################################################
 # Function for finding the serial ports
def find_out_serial_ports():
    system_name = platform.system()
    if system_name == "Windows":
        # Scan for available ports.
        available = []
        for i in range(256):
            #print("Trying ",i)
            try:
                s = serial.Serial(i, 9600, 
                                  timeout=1,
                                  xonxoff=True,
                                  writeTimeout=1,
                                  interCharTimeout=1,
                                  stopbits=1 )
                print("Opening ",i)
                available.append('COM'+str(i+1))
                print("Closing ",i)
                s.close()
            except serial.SerialException:
                #print("Error for port ",i)
                pass
        return available
    elif system_name == "Darwin":
        # Mac
        return glob.glob('/dev/tty*') + glob.glob('/dev/cu*')
    else:
        import glob
        # Assume Linux or something else
        return glob.glob('/dev/ttyUSB*')
#############################################################

#############################################################
  # Function : Opening the selected serial port        


def version_check(ser, version):
    #print("Checking version...")
    firmware_reg = get_reg_u(31, ser)
    firmware_version = str(firmware_reg)
    firmware_version = firmware_version[:-2] + "." + firmware_version[-2:] # insert decimal point
    firmware_major = firmware_reg // 100 # integer division
    gui_major = int(version[:-3])
    # print(firmware_version,gui_version,firmware_major,gui_major)
    if firmware_version == version:
        return None
    elif firmware_major == gui_major:
        return "Warning: version mismatch between GUI %s and FPGA firmware %s. Please upgrade soon." \
               % (version, firmware_version)
    else:
        if firmware_major < gui_major:
            to_upgrade = "FPGA firmware"
        else:
            to_upgrade = "GUI"
        return "Error: GUI version %s does not match FPGA firmware version %s." \
               "Things may fail unless you upgrade the %s!" % (version, firmware_version, to_upgrade)


def open_serial_port(portname):
    if len(portname) == 0:
        raise ConnectionException("No Port selected")
    else:
        baudrate = 3000000
        if platform.system() == "Windows":
            portNrstr = portname[3:]
            print(portNrstr)
            portNrint = int(portNrstr)-1
            print(str(portNrint))
            ser = serial.Serial(portNrint, baudrate,
                                timeout=2,
                                writeTimeout=2,
                                interCharTimeout=2,
                                xonxoff=False,
                                stopbits=1 )
        else:
            ser = serial.Serial(portname, baudrate,
                                timeout=2,
                                writeTimeout=2,
                                interCharTimeout=2,
                                xonxoff=False,
                                stopbits=1 )

        # Verification of board readback and versions

        #print("Testing read/write to regs...")
        # Write, to initialise the FTDI
        for k in range(16,31):
            set_reg_u(k,0,ser)

        # Store FPGA regs temporarily
        temp_14 = get_reg_u(14,ser)
        temp_15 = get_reg_u(15,ser)

        # Write in test pattern
        set_reg_u(14, 0x5c5c, ser) # bit pattern of 0101_1010_0101_1010
        set_reg_u(15, 0xc5c5, ser) # bit pattern of 1010_0101_1010_0101

        # Readback
        #print("Readback...")
        if get_reg_u(14, ser) == 0x5c5c and get_reg_u(15, ser) == 0xc5c5:
            #print("Success.")

            # Write original values back in
            set_reg_u(14, temp_14, ser)
            set_reg_u(15, temp_15, ser)
        else:
            # Readback error
            raise ConnectionException("Error: registers do not match readback.\n"
                                      "Please close this program, powercycle EVIL and try again.")

        return ser
