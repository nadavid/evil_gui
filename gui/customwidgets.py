from PyQt4 import QtGui as QtG
from PyQt4.QtCore import pyqtSlot


class SampleRateSpinBox(QtG.QSpinBox):
    # we have to invert our values so that the spin buttons and mousewheel spins in the correct direction
    def value(self):
        return self.maximum() - QtG.QSpinBox.value(self)

    def setValue(self, value):
        QtG.QSpinBox.setValue(self, self.maximum() - value)

    def sample_rate(self):
        t0 = 4.13541668e-3  # ms, minimum sampling period
        t1 = t0*512/400     # ms, sampling period/n
        return 1/(t0 + t1*self.value())

    def textFromValue(self, value):
        return "%.2f" % self.sample_rate()


class TTLExpansionSpinBox(QtG.QSpinBox):
    def textFromValue(self, value):
        dt = 1000 * 2048. / 96000000 # since we use a 11bit gated clock and want ms as unit
        return "%.2f" % (dt*value)