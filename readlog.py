import sys

from PyQt4 import QtCore as QtC
from PyQt4 import QtGui as QtG
from PyQt4.QtCore import pyqtSlot, pyqtSignal
from PyQt4.uic import loadUi

import numpy as np
from pyqtgraph.functions import mkColor


class Plotty(QtG.QWidget):
    def __init__(self):
        QtG.QWidget.__init__(self)

        loadUi('gui/plotty.ui', self)
        filename = QtG.QFileDialog.getOpenFileName(self, filter="EVIL file (*.evl);;All files(*)")

        self.plots = []

        with open(filename, 'rb') as f:
            while True:
                try:
                    self.plots.append({'data': np.load(f)})
                except IOError:
                    break

        row = 1
        for p in self.plots:
            color = mkColor(row)

            p['name'] = QtG.QLineEdit()
            p['name'].setText('Plot %d'% row)
            p['name'].setStyleSheet('QLineEdit{background-color: %s}' % color.name())
            self.gridLayout.addWidget(p['name'], row, 0)

            p['slider'] = QtG.QSlider(QtC.Qt.Horizontal)
            p['slider'].valueChanged.connect(self.start_changed)
            self.gridLayout.addWidget(p['slider'], row, 1)

            p['show'] = QtG.QCheckBox()
            p['show'].setCheckState(True)
            p['show'].setTristate(False)
            p['show'].stateChanged.connect(self.plot_toggled)
            self.gridLayout.addWidget(p['show'], row, 2)

            p['start'] = 0

            p['plot'] = self.plotWidget.getPlotItem().plot(p['data'][1], pen=row, antialias=True)

            row += 1

        minlength = -1
        for p in self.plots:
            if p['data'].shape[1] < minlength or minlength == -1:
                minlength = p['data'].shape[1]
        self.lengthSlider.setMaximum(minlength)
        self.lengthSlider.setValue(self.lengthSlider.maximum())
        self.lengthSlider.valueChanged.connect(self.length_changed)
        self.length_changed(self.lengthSlider.value())

    @pyqtSlot(int)
    def length_changed(self, value):
        for p in self.plots:
            max = p['data'].shape[1] - value
            p['slider'].setEnabled(max > 0)
            p['slider'].setMaximum(max if max > 0 else 1)

    @pyqtSlot(int)
    def plot_toggled(self, state):
        for p in self.plots:
            if not p['show'] == self.sender():
                continue

            if state == QtC.Qt.Checked:
                self.plotWidget.getPlotItem().addItem(p['plot'])
            else:
                self.plotWidget.getPlotItem().removeItem(p['plot'])

    @pyqtSlot(int)
    def start_changed(self, start):
        length = self.lengthSlider.value()
        for p in self.plots:
            if not p['slider'] == self.sender():
                continue

            p['plot'].setData(p['data'][1, start:start+length])

app = QtG.QApplication(sys.argv)
pt = Plotty()
pt.show()
QtG.QApplication.exec_()